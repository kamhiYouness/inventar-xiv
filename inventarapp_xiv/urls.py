from django.urls import path
from . import views

app_name = 'inventar'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register_view, name='register'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),

    path('material-handover/', views.material_handover_view, name='material-handover'),
    path('administration/<str:pk>', views.administration_view, name='administration'),
    path('administration/', views.administration_view, name='administration'),

    path('material-handover/<str:pk>/delete/', views.MaterialHandoverDelete.as_view(), name='material-handover-delete'),
    path('material-handover/<str:pk>/edit/', views.MaterialHandoverEdit.as_view(), name='material-handover-edit'),
    path('material-handover/create/', views.MaterialHandoverCreate.as_view(), name='material-handover-create'),

    path('employee/<str:pk>/delete/', views.EmployeeDelete.as_view(), name='employee-delete'),
    path('employee/<str:pk>/edit/', views.EmployeeEdit.as_view(), name='employee-edit'),
    path('employee/create/', views.EmployeeCreate.as_view(), name='employee-create'),

    path('credentials/<str:pk>/delete/', views.CredentialsDelete.as_view(), name='credentials-delete'),
    path('credentials/<str:pk>/edit/', views.CredentialsEdit.as_view(), name='credentials-edit'),
    path('credentials/create/', views.CredentialsCreate.as_view(), name='credentials-create'),

    path('hardware/<str:pk>/delete/', views.HardwareDelete.as_view(), name='hardware-delete'),
    path('hardware/<str:pk>/edit/', views.HardwareEdit.as_view(), name='hardware-edit'),
    path('hardware/create/', views.HardwareCreate.as_view(), name='hardware-create'),

    path('sim/<str:pk>/delete/', views.SimCardDelete.as_view(), name='sim-card-delete'),
    path('sim/<str:pk>/edit/', views.SimCardEdit.as_view(), name='sim-card-edit'),
    path('sim/create/', views.SimCardCreate.as_view(), name='sim-card-create'),

    path('key/<str:pk>/delete/', views.KeyDelete.as_view(), name='key-delete'),
    path('key/<str:pk>/edit/', views.KeyEdit.as_view(), name='key-edit'),
    path('key/create/', views.KeyCreate.as_view(), name='key-create'),

    path('others/<str:pk>/delete/', views.OthersDelete.as_view(), name='others-delete'),
    path('others/<str:pk>/edit/', views.OthersEdit.as_view(), name='others-edit'),
    path('others/create/', views.OthersCreate.as_view(), name='others-create'),

]
