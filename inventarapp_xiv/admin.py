from django.contrib import admin

from .models import Employee
from .models import InitialCredentials
from .models import Hardware
from .models import SimCard
from .models import Key
from .models import Others
from .models import MaterialHandover

admin.site.register(Employee)
admin.site.register(InitialCredentials)
admin.site.register(Hardware)
admin.site.register(SimCard)
admin.site.register(Key)
admin.site.register(Others)
admin.site.register(MaterialHandover)
