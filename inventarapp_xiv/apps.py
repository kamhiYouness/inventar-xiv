from django.apps import AppConfig


class InventarappXivConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'inventarapp_xiv'
