from django.db.models import Q

from .forms import LoginForm, RegisterForm
from .models import Employee, InitialCredentials, Hardware, SimCard, Key, Others, MaterialHandover
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import DeleteView, UpdateView, CreateView


def index(request):
    return render(request, 'inventar/base.html')


def register_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('inventar:index'))
    register_form = RegisterForm()
    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            username = register_form.cleaned_data["username"]
            password = register_form.cleaned_data["password"]
            email = register_form.cleaned_data["email"]
            User.objects.create_user(username=username, password=password, email=email)
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                if request.user.is_superuser:
                    return redirect(reverse('inventar:admin'))
                else:
                    return redirect(reverse('inventar:index'))
    return render(request, 'inventar/register.html', {'register_form': register_form})


def login_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('inventar:index'))
    login_form = LoginForm()
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data["username"]
            password = login_form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                if request.user.is_superuser:
                    return redirect(reverse('inventar:admin'))
                else:
                    return redirect(reverse('inventar:index'))

    return render(request, 'inventar/login.html', {'login_form': login_form})


@login_required
def logout_view(request):
    logout(request)
    return redirect(reverse('inventar:index'))


@login_required
def administration_view(request, pk='employee'):
    if request.GET.get('q') == None:
        query = ''
    else:
        query = request.GET.get('q')

    if pk == 'employee':
        employees = filter_employee(request)
    else:
        employees = Employee.objects.all()

    if pk == 'credentials':
        credential_list = filter_credentials(request)
        credential_list = filter_credentials_exists(credential_list)

    else:
        credential_list = InitialCredentials.objects.all()
        credential_list = filter_credentials_exists(credential_list)

    if pk == 'hardware':
        hardware_list = filter_hardware(request)
        hardware_list = filter_hardware_exists(hardware_list)
    else:
        hardware_list = Hardware.objects.all()
        hardware_list = filter_hardware_exists(hardware_list)

    if pk == 'sim':
        sim_card_list = filter_sim_cards(request)
        sim_card_list = filter_sim_exists(sim_card_list)
    else:
        sim_card_list = SimCard.objects.all()
        sim_card_list = filter_sim_exists(sim_card_list)

    if pk == 'key':
        key_list = filter_key(request)
        key_list = filter_key_exists(key_list)
    else:
        key_list = Key.objects.all()
        key_list = filter_key_exists(key_list)

    if pk == 'others':
        other_list = filter_others(request)
        other_list = filter_others_exists(other_list)
    else:
        other_list = Others.objects.all()
        other_list = filter_others_exists(other_list)

    context = {
        'query': query,
        'active_tab': pk,
        'employees': employees,
        'credential_list': credential_list,
        'hardware_list': hardware_list,
        'sim_card_list': sim_card_list,
        'key_list': key_list,
        'other_list': other_list,
    }

    return render(request, 'inventar/administration.html', context)


def is_valid_queryparam(param):
    return param != '' and param is not None


def filter_employee(request):
    employees = Employee.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        employees = employees.filter(
            Q(personnel_number__contains=_query) | Q(first_name__contains=_query) | Q(last_name__contains=_query) | Q(
                email__contains=_query) | Q(phone__contains=_query) | Q(abbreviation__contains=_query) | Q(
                contract_type__contains=_query) | Q(status__contains=_query))
    return employees


def filter_credentials(request):
    credential_list = InitialCredentials.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        credential_list = credential_list.filter(
            Q(account_type__contains=_query) | Q(username__contains=_query))
    return credential_list


def filter_hardware(request):
    hardware_list = Hardware.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        hardware_list = hardware_list.filter(
            Q(hardware_type__contains=_query) | Q(description__contains=_query) | Q(serial_nummer__contains=_query) | Q(
                hint__contains=_query))

    return hardware_list


def filter_sim_cards(request):
    sim_card_list = SimCard.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        sim_card_list = sim_card_list.filter(
            Q(phone_number__contains=_query) | Q(card_number__contains=_query))

    return sim_card_list


def filter_key(request):
    key_list = Key.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        key_list = key_list.filter(
            Q(key_number__contains=_query) | Q(hint__contains=_query))

    return key_list


def filter_others(request):
    other_list = Others.objects.all()

    _query = request.GET.get('q')

    if is_valid_queryparam(_query):
        other_list = other_list.filter(
            Q(description__contains=_query) | Q(hint__contains=_query))

    return other_list


def filter_credentials_exists(list):
    filtered_list = []

    for credentials in list:
        credentials.borrowed = MaterialHandover.objects.filter(initial_credentials__pk=credentials.pk).exists()
        filtered_list.append(credentials)

    return filtered_list


def filter_hardware_exists(list):
    filtered_list = []

    for hardware in list:
        hardware.borrowed = MaterialHandover.objects.filter(hardware__pk=hardware.pk).exists()
        filtered_list.append(hardware)

    return filtered_list


def filter_sim_exists(list):
    filtered_list = []

    for sim in list:
        sim.borrowed = MaterialHandover.objects.filter(sim_card__pk=sim.pk).exists()
        filtered_list.append(sim)

    return filtered_list


def filter_key_exists(list):
    filtered_list = []

    for key in list:
        key.borrowed = MaterialHandover.objects.filter(key__pk=key.pk).exists()
        filtered_list.append(key)

    return filtered_list


def filter_others_exists(list):
    filtered_list = []

    for other in list:
        other.borrowed = MaterialHandover.objects.filter(others__pk=other.pk).exists()
        filtered_list.append(other)

    return filtered_list


class EmployeeCreate(CreateView):
    model = Employee
    fields = [
        'personnel_number',
        'first_name',
        'last_name',
        'email',
        'phone',
        'abbreviation',
        'contract_type',
        'status']
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class EmployeeEdit(UpdateView):
    model = Employee
    fields = [
        'personnel_number',
        'first_name',
        'last_name',
        'email',
        'phone',
        'abbreviation',
        'contract_type',
        'status']
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class EmployeeDelete(DeleteView):
    model = Employee
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(EmployeeDelete, self).post(request, *args, **kwargs)


class CredentialsCreate(CreateView):
    model = InitialCredentials
    fields = [
        'account_type',
        'username',
        'password',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class CredentialsEdit(UpdateView):
    model = InitialCredentials
    fields = [
        'account_type',
        'username',
        'password',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class CredentialsDelete(DeleteView):
    model = InitialCredentials
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(CredentialsDelete, self).post(request, *args, **kwargs)


class HardwareCreate(CreateView):
    model = Hardware
    fields = [
        'hardware_type',
        'description',
        'serial_nummer',
        'hint',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class HardwareEdit(UpdateView):
    model = Hardware
    fields = [
        'hardware_type',
        'description',
        'serial_nummer',
        'hint',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class HardwareDelete(DeleteView):
    model = Hardware
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(HardwareDelete, self).post(request, *args, **kwargs)


class SimCardCreate(CreateView):
    model = SimCard
    fields = [
        'phone_number',
        'card_number',
        'PIN',
        'PUK',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class SimCardEdit(UpdateView):
    model = SimCard
    fields = [
        'phone_number',
        'card_number',
        'PIN',
        'PUK',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class SimCardDelete(DeleteView):
    model = SimCard
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(SimCardDelete, self).post(request, *args, **kwargs)


class KeyCreate(CreateView):
    model = Key
    fields = [
        'key_number',
        'hint',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class KeyEdit(UpdateView):
    model = Key
    fields = [
        'key_number',
        'hint',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class KeyDelete(DeleteView):
    model = Key
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(KeyDelete, self).post(request, *args, **kwargs)


class OthersCreate(CreateView):
    model = Others
    fields = [
        'description',
        'hint',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class OthersEdit(UpdateView):
    model = Others
    fields = [
        'description',
        'hint',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class OthersDelete(DeleteView):
    model = Others
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(OthersDelete, self).post(request, *args, **kwargs)


@login_required
def material_handover_view(request):
    material_handovers = MaterialHandover.objects.all
    context = {
        'material_handovers': material_handovers
    }
    return render(request, 'inventar/material-handover.html', context)


class MaterialHandoverCreate(CreateView):
    model = MaterialHandover
    fields = [
        'employee',
        'initial_credentials',
        'hardware',
        'sim_card',
        'key',
        'others',
        'created_date',
        'returning_date',
    ]
    template_name = 'inventar/create.html'
    success_url = reverse_lazy('inventar:administration')


class MaterialHandoverEdit(UpdateView):
    model = MaterialHandover
    fields = [
        'employee',
        'initial_credentials',
        'hardware',
        'sim_card',
        'key',
        'others',
        'created_date',
        'returning_date',
    ]
    template_name = 'inventar/edit.html'
    success_url = reverse_lazy('inventar:administration')


class MaterialHandoverDelete(DeleteView):
    model = MaterialHandover
    template_name = 'inventar/delete.html'
    success_url = reverse_lazy('inventar:administration')

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(MaterialHandoverDelete, self).post(request, *args, **kwargs)
