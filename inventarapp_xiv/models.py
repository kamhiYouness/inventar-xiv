from django.utils import timezone
from django.db import models

CHOICES_CONTRACT_TYPE = [
    ('Werkstudent', 'Werkstudent'),
    ('Angestellter', 'Angestellter'),
]

CHOICES_EMPLOYEE_STATUS = [
    ('Aktiv', 'Aktiv'),
    ('Inaktiv', 'Inaktiv'),
]

CHOICES_ACCOUNT_TYPE = [
    ('MacBook', 'MacBook'),
    ('Apple_ID', 'Apple ID'),
    ('Office_365', 'Office 365'),
    ('Egencia', 'Egencia'),
    ('BCS', 'BCS'),
]

CHOICES_HARDWARE_TYPE = [
    ('Handy', 'Handy'),
    ('Notebook', 'Notebook'),
    ('Festplatte', 'Festplatte'),
    ('Zubehör', 'Zubehör'),
]


class Employee(models.Model):
    personnel_number = models.CharField(max_length=20, primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=50)
    abbreviation = models.CharField(max_length=20)
    contract_type = models.CharField(max_length=50, choices=CHOICES_CONTRACT_TYPE)
    status = models.CharField(max_length=20, choices=CHOICES_EMPLOYEE_STATUS)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class InitialCredentials(models.Model):
    account_type = models.CharField(max_length=30, choices=CHOICES_ACCOUNT_TYPE)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.account_type + '/' + self.username


class Hardware(models.Model):
    hardware_type = models.CharField(max_length=15, choices=CHOICES_HARDWARE_TYPE)
    description = models.TextField()
    serial_nummer = models.CharField(max_length=20)
    hint = models.CharField(max_length=100)

    def __str__(self):
        return self.hardware_type + '/' + self.serial_nummer


class SimCard(models.Model):
    phone_number = models.CharField(max_length=50)
    card_number = models.CharField(max_length=20)
    PIN = models.CharField(max_length=10)
    PUK = models.CharField(max_length=10)

    def __str__(self):
        return self.phone_number


class Key(models.Model):
    key_number = models.CharField(max_length=50)
    hint = models.CharField(max_length=100)

    def __str__(self):
        return self.key_number


class Others(models.Model):
    description = models.TextField()
    hint = models.CharField(max_length=100)

    def __str__(self):
        return self.description


class MaterialHandover(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    initial_credentials = models.ManyToManyField(InitialCredentials, blank=True, null=True)
    hardware = models.ManyToManyField(Hardware, blank=True, null=True)
    sim_card = models.ManyToManyField(SimCard, blank=True, null=True)
    key = models.ManyToManyField(Key, blank=True, null=True)
    others = models.ManyToManyField(Others, blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    returning_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.employee.personnel_number
